<?php

namespace Database\Seeders\Auth;

use App\Domains\Auth\Models\Wallet;
use Illuminate\Database\Seeder;

class WalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Wallet::create([
            'balance'=>0
        ]);
    }
}
