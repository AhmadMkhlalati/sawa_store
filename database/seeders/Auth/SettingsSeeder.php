<?php

namespace Database\Seeders\Auth;

use App\Domains\Auth\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'key'=>'usd',
            'value'=>'13000'
        ]);
    }
}
