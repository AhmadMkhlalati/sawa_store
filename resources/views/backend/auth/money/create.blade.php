@inject('model', '\App\Domains\Auth\Models\User')

@extends('backend.layouts.app')

@section('title', __('Create Money Transfer'))

@section('content')

    <livewire:frontend.get-transfer-data />
@endsection
