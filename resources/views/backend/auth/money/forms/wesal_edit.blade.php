<div class="dropdown">
    <button class="btn btn-primary btn-text dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
       {{ __('Operation')}}
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
        <button type="button"class="dropdown-item" data-toggle="modal" data-target="#ModalMakeTransfer">
            {{__("Process Money Order")}}
        </button>
    </div>
</div>






<!-- Modal -->
<div class="modal fade" id="ModalMakeTransfer" tabindex="-1" role="dialog" aria-labelledby="ModalMakeTransferLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Process Money Order')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{__('we will send money to this order')}}
            <ul>
                <li><b>{{__('Beneficiary Name')}}</b><p>{{$data->beneficiary_name}}</p></li>
                <li><b>{{__('Beneficiary Phone')}}</b><p>{{$data->beneficiary_no}}</p></li>
                <li><b>{{__('Amount')}}</b><p>{{$data->amount}}</p></li>
                <li><b>{{__('Currency')}}</b><p>{{$data->currency}}</p></li>
                <li><b>{{__('Delivery Amount')}}</b><p>{{$data->amount_sy}}</p></li>
                <li><b>{{__('Status')}}</b><p>{{$data->status}}</p></li>
            </ul>
                {{__('Are You Sure You want to change')}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('No')}}</button>
                <button type="button" class="btn btn-primary" wire:click="ConfirmTransferWesal({{$data->id}})" data-dismiss="modal">{{__('Yes')}}</button>
            </div>
        </div>
    </div>
</div>
