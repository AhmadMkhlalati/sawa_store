<div>
    <x-forms.post :action="route('admin.auth.money.store')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create Money Transfer')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="route('admin.auth.money.index')" :text="__('Cancel')" />
            </x-slot>

            <x-slot name="body">
                <div x-data="{userType : ''}">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Reference Number')</label>

                        <div class="col-md-10">
                            <input type="text" name="reference_number" wire:model="code" class="form-control" placeholder="{{ __('Reference Number') }}" value="{{ old('reference_number') }}"  required />
                            <button class="btn btn-sm btn-flickr mt-2" type="button" wire:click="getMoneyData()" >
                                    @lang('Get Data')
                            </button>
                        </div>

                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Beneficiary Name')</label>

                        <div class="col-md-10">
                            <input type="text" name="beneficiary_name" value="{{$beneficiary_name}}" class="form-control" placeholder="{{ __('Beneficiary Name') }}" value="{{ old('beneficiary_name') }}"  required />
                        </div>
                    </div><!--form-group-->



                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label">@lang('Beneficiary Phone')</label>

                        <div class="col-md-10">
                            <input type="text" name="beneficiary_phone" value="{{$beneficiary_no}}" class="form-control" placeholder="{{ __('Beneficiary Phone') }}" value="{{ old('Beneficiary Phone') }}" maxlength="12" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label">@lang('Amount')</label>

                        <div class="col-md-10">
                            <input type="text" name="delivery_amount" value="{{$amount_sy}}" class="form-control" placeholder="{{ __('Amount') }}" value="{{ old('Amount') }}" maxlength="12" required />
                        </div>
                    </div><!--form-group-->




                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create Money Transfer')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post></div>
