@extends('backend.layouts.app')

@section('title', __('Wesal Money Management'))

@section('breadcrumb-links')
    @include('backend.auth.money.includes.breadcrumb-links')
@endsection

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Wesal Money Management')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action"
                    :href="route('admin.auth.money.create')"
                    :text="__('Create Money Transfer')"
                    permission="admin.access.money.create"
                />
            </x-slot>
        @endif

        <x-slot name="body">
            <livewire:backend.wesal-money-table />
        </x-slot>
    </x-backend.card>
@endsection
