<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Auth\Models\User;
use App\Domains\Auth\Models\WesalMoney;
use App\Domains\Auth\Services\MoneyService;
use App\Domains\Auth\Services\PermissionService;
use App\Domains\Auth\Services\RoleService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class MoneyTable.
 */
class WesalMoneyTable extends DataTableComponent
{
    /**
     * @var
     */
    public $status;


    /**
     * @var array|string[]
     */
    public array $sortNames = [
        'email_verified_at' => 'Verified',
        'two_factor_auth_count' => '2FA',
    ];



    /**
     * @var array|string[]
     */
    public array $filterNames = [
        'beneficiary_name' => 'Beneficiary Name',
    ];

    /**
     * @param  string  $status
     */
    public function mount($status = 'active'): void
    {
        $this->status = $status;
    }

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $query = WesalMoney::latest()->select();
        return $query
            ->when($this->getFilter('search'), fn ($query, $term) => $query->search($term));

    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return [];
    }

    public function ConfirmTransferWesal($id){

        $wesalOrder = WesalMoney::find($id);

        $data = ['beneficiary_name'=>$wesalOrder['beneficiary_name'],'beneficiary_phone'=>$wesalOrder['beneficiary_no'],'delivery_amount'=>$wesalOrder['amount_sy'],'reference_number'=>$wesalOrder['id']];
        $user = Auth::user();
        $user->syncPermissions(['admin.access.money.create']);

        $order = MoneyService::store($data);

//        session()->flash('flash_warning', __('The transfer money order was successfully created.'));


        return redirect()->route('admin.auth.money.wesal.index');


    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [

            Column::make(__('#'),'id')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Beneficiary Name'),'beneficiary_name')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Beneficiary Phone'),'beneficiary_no')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Customer Id'),'customer_id')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Amount'),'amount')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Delivery Amount'),'amount_sy')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Currency'),'currency')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Status'),'status')
                ->sortable()
                ->searchable()
            ,Column::make(__('Response'),'response')
                ->sortable()
                ->searchable()
            ,Column::make(__('Payment Method'),'payment_method')
                ->sortable()
                ->searchable()
            ,Column::make(__('Notes'),'notes')
                ->sortable()
                ->searchable(),
            Column::make(__('created_at'),'created_at')
                ->sortable()
            ,
            Column::make(__('updated_at'),'updated_at')
                ->sortable()
            ,
            Column::make('Action')
                ->format(function ($value, $column, $row) {
                    return view('backend.auth.money.forms.wesal_edit', ['data' => $row]);
                }),


        ];
    }


}
