<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Auth\Models\Money;
use App\Domains\Auth\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class MoneyTable.
 */
class MoneyTable extends DataTableComponent
{
    /**
     * @var
     */
    public $status;


    /**
     * @var array|string[]
     */
    public array $sortNames = [
        'email_verified_at' => 'Verified',
        'two_factor_auth_count' => '2FA',
    ];

    /**
     * @var array|string[]
     */
    public array $filterNames = [
        'beneficiary_name' => 'Beneficiary Name',
        'transaction_UID' => 'transaction_UID',
    ];

    /**
     * @param  string  $status
     */
    public function mount($status = 'active'): void
    {
        $this->status = $status;
    }

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $query = Money::select();
        return $query
            ->when($this->getFilter('search'), fn ($query, $term) => $query->search($term));

    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [

            Column::make(__('Beneficiary Name'),'beneficiary_name')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Beneficiary Phone'),'beneficiary_phone')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Reference Number'),'reference_number')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Delivery Amount'),'delivery_amount')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('Transaction UID'),'transaction_UID')
                ->sortable()
                ->searchable()
            ,
            Column::make(__('created_at'),'created_at')
                ->sortable()
            ,
            Column::make(__('updated_at'),'updated_at')
                ->sortable()
            ,


        ];
    }


}
