<?php

namespace App\Http\Livewire\Frontend;

use App\Domains\Auth\Models\WesalMoney;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Livewire\Component;
use function _PHPStan_4d77e98e1\React\Async\await;

/**
 * Class GetTransferData.
 */
class GetTransferData extends Component
{
    /**
     * @var
     */
    public $code;
    public $beneficiary_name,$beneficiary_no,$amount_sy;
    public $loading= false;




    /**
     * @param  Request  $request
     * @return mixed
     */
     public function getMoneyData()
    {
        $this->loading = true;
        if($this->code!=null){
            $data = WesalMoney::find($this->code);

            // Assuming the response is JSON, decode it and store it in the $data property
            $this->beneficiary_name = $data['beneficiary_name'];
            $this->beneficiary_no = $data['beneficiary_no'];
            $this->amount_sy = $data['amount_sy'];
        }
        $this->loading = false;

        session()->flash('flash_success', __('data fetch successfully'));




    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        return view('backend.auth.money.forms.create');
    }
}
