<?php


namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Http;

class FormWithHttpData extends Component
{
    public $data = [];
    public $moneyId; // Add a property to store the user-provided ID

    public function __construct()
    {
        $this->data->beneficiary_name =null;
        $this->data->beneficiary_no =null;
        $this->data->amount_sy =null;
    }



    public function render()
    {
        return view('backend.auth.money.forms.create');
    }
}
