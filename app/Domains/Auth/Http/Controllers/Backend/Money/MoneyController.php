<?php

namespace App\Domains\Auth\Http\Controllers\Backend\Money;

//use App\Domains\Money\Http\Requests\Backend\User\DeleteUserRequest;
//use App\Domains\Money\Http\Requests\Backend\User\EditUserRequest;
//use App\Domains\Money\Http\Requests\Backend\User\StoreUserRequest;
//use App\Domains\Money\Http\Requests\Backend\User\UpdateUserRequest;
use App\Domains\Auth\Services\MoneyService;
use App\Domains\Auth\Services\PermissionService;
use App\Domains\Auth\Services\RoleService;
use App\Domains\Money\Http\Controllers\Backend\StoreUserRequest;
use App\Domains\Money\Http\Controllers\Backend\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController.
 */
class MoneyController
{
    /**
     * @var MoneyService
     */
    protected $moneyservice;

    /**
     * @var RoleService
     */
    protected $roleService;

    /**
     * @var PermissionService
     */
    protected $permissionService;

    /**
     * UserController constructor.
     *
     * @param  MoneyService  $moneyservice
     * @param  RoleService  $roleService
     * @param  PermissionService  $permissionService
     */
    public function __construct(MoneyService $moneyservice, RoleService $roleService, PermissionService $permissionService)
    {
        $this->moneyService = $moneyservice;
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.auth.money.index');
    }


    public function wesal_money()
    {
        return view('backend.auth.money.wesal_index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.auth.money.create')
            ->withRoles($this->roleService->get())
            ->withCategories($this->permissionService->getCategorizedPermissions())
            ->withGeneral($this->permissionService->getUncategorizedPermissions());
    }

    /**
     * @param  StoreUserRequest  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $data = $request->validate(['beneficiary_name'=>'required','beneficiary_phone'=>'required','delivery_amount'=>'required','reference_number'=>'required']);
        $user = Auth::user();
        $user->syncPermissions(['admin.access.money.create']);

        $order = $this->moneyService->store($data);

        return redirect()->route('admin.auth.money.create', $order)->withFlashSuccess(__('The transfer money order was successfully created.'));
    }

    /**
     * @param  User  $user
     * @return mixed
     */
    public function show(User $user)
    {
        return view('backend.auth.user.show')
            ->withUser($user);
    }



}
