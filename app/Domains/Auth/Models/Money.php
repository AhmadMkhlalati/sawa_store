<?php

namespace App\Domains\Auth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Money extends Model
{
    use HasFactory;
    protected $fillable = [
        'beneficiary_name','beneficiary_phone','reference_number','delivery_amount','transaction_UID'
    ];

    public function scopeSearch($query, $term)
    {
        return $query->where(function ($query) use ($term) {
            $query->where('beneficiary_name', 'like', '%'.$term.'%')
                ->orWhere('beneficiary_phone', 'like', '%'.$term.'%');
        });
    }


}
