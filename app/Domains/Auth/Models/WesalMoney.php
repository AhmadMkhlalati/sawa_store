<?php

namespace App\Domains\Auth\Models;

use App\Domains\Auth\Models\Currency;
use App\Domains\Auth\Models\Customer;
use App\Domains\Auth\Models\MoneyResponse;
use App\Domains\Auth\Models\MoneyUpdate;
use App\Domains\Auth\Models\Settings;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WesalMoney extends Model
{
    use HasFactory;

    protected $connection= 'wesal';

    protected $table = 'moneys';

    protected $fillable = [
        'id',
        'customer_id',
        'amount',
        'amount_sy',
        'currency',
        'status',
        'response',
        'payment_method',
        'payment_name',
        'beneficiary_no',
        'company_name',
        'beneficiary_name',
        'created_at',
        'updated_at',
        'active',
        'notes',
        'deleted_at',
        "update_permission",
        "message_delivery_time_en",
        "message_delivery_time_ar",
        "paid_at",
        "confirmed_at",
        "calculated"
    ];

    protected $filter_list = ['id', 'customer_id', 'amount', 'amount_sy', 'currency', 'status', 'response', 'payment_method', 'beneficiary_no', 'company_name', 'beneficiary_name', 'created_at', 'updated_at', 'active', 'notes', 'deleted_at'];

    public function scopeSearch($query, $term)
    {
        return $query->where(function ($query) use ($term) {
            $query->where('beneficiary_name', 'like', '%'.$term.'%')
                ->orWhere('beneficiary_no', 'like', '%'.$term.'%')
                ->orWhere('customer_id', 'like', '%'.$term.'%')
                ->orWhere('amount', 'like', '%'.$term.'%')
                ->orWhere('status', 'like', '%'.$term.'%')
                ->orWhere('amount_sy', 'like', '%'.$term.'%');
        });
    }

    // Custom method for currency conversion
    public static function convertAmountToCurrency($amount, $currenct_currency, $payment_method)
    {
        //HaramUSD
        $usd = Settings::where('key', 'usd_exchange_haram')->first();

        $currency = Currency::where('code', $currenct_currency)->first();


        if ($amount >= $currency->min && $amount < $currency->avg) {

            $amount_sy = round($amount * $currency->rate_to_sp / (2 - $currency['fee_lower_' . $payment_method]) - $currency['tax_lower_' . $payment_method] * $usd->value);
        } else {
            $amount_sy = round($amount * $currency->rate_to_sp / (2 - $currency['fee_upper_' . $payment_method]));
        }
        //dd($usd,$amount,$amount_sy);
        return $amount_sy;
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency', 'code')->with('field');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function money_updates()
    {
        return $this->hasMany(MoneyUpdate::class);
    }

    public function response_relation()
    {
        return $this->hasOne(MoneyResponse::class);
    }

}
