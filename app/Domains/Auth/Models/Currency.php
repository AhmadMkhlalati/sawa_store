<?php

namespace App\Domains\Auth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $connection= 'wesal';


    protected $fillable = ['name', 'code', 'rate_to_sp', 'duration_paypal', 'symbol', 'duration_wise', "icon","type","end_customer_id","has_wallet"];
    protected $hidden = ["created_at", "updated_at"];

    public function field()
    {
        return $this->hasMany(CurrenciesField::class);
    }

    public function getDurationPaypalAttribute($value)
    {
        return $value ? ['message_ar' => explode("|", $value)[0], 'message_en' => explode("|", $value)[1]] : $value;

    }

    public function getDurationWiseAttribute($value)
    {
        return $value ? ['message_ar' => explode("|", $value)[0], 'message_en' => explode("|", $value)[1]] : $value;
    }

    public function calc_validation($data)
    {
        $this->tax = (int)$data[0]->value*(int)$data[1]->value;

        //max
        $this->paypal_max_syp =  round($this->max * $this->rate_to_sp / (2 - $this['fee_upper_paypal']));
        $this->wise_max_syp =  round($this->max * $this->rate_to_sp / (2 - $this['fee_upper_wise']));

        //min
        $this->paypal_min_syp =  round($this->min * $this->rate_to_sp / (2 - $this['fee_lower_paypal']) - $this['tax_lower_paypal'] * $data[1]->value);
        $this->wise_min_syp =  round($this->min * $this->rate_to_sp / (2 - $this['fee_lower_wise']) - $this['tax_lower_wise'] * $data[1]->value);
    }





}
