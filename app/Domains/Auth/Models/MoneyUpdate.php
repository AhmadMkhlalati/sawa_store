<?php

namespace App\Domains\Auth\Models;

use App\Filters\Filter;
use App\Models\Money;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoneyUpdate extends Model
{
    use HasFactory;

    protected $connection= 'wesal';


    protected $guarded = [];
    protected $filter_list = [
        "beneficiary_name",
        "beneficiary_no",
        "money_id",
        "status",
    ];

    public function scopeFilter($query, $filters)
    {
        return Filter::filter($query, $filters, $this->filter_list);
    }

    public function money()
    {
        return $this->belongsTo(Money::class);
    }
}
