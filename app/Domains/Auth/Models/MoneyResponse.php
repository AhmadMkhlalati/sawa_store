<?php

namespace App\Domains\Auth\Models;

use App\Models\Money;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoneyResponse extends Model
{
    use HasFactory;

    protected $connection= 'wesal';


    protected $fillable= [
        'id',	'money_id',	'result','payment_method',	'created_at',	'updated_at'
    ];

    public function getResultAttribute(){
        $money = Money::find($this->attributes['money_id']);
        if($money->payment_method == 'paypal'){
            try {
                return  explode(',',explode('id: ',explode('sale:', $this->attributes['result'])[1])[1])[0];
            }catch (\Exception $e){

               return  json_decode($this->attributes['result'])->transactions[0]->related_resources[0]->sale->id;
            }
        }
        try {

            return $money->payment_method =='credit_card' ? gettype(json_decode($this->attributes['result'])) == 'object' || gettype(json_decode($this->attributes['result'])) == 'string'? json_decode(json_decode($this->attributes['result']))->purchase_units[0]->payments->captures[0]->id :"an error ":"yahea old response";
        }catch (\Exception $e){
            return $this->attributes['result'];
        }
    }
}
