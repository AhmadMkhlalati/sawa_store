<?php

namespace App\Domains\Auth\Models;

use App\Filters\Filter;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Model;

class Customer extends Model
{
    use HasFactory, HasApiTokens, Notifiable, Billable;

    protected $connection= 'wesal';

    protected $filter_list = ['first_name', 'last_name', 'email', 'phone', 'country', 'birthday', 'gender', 'authToken', 'fcm_token', 'vesion', 'email_verified_at', 'confirm_code', 'confirmed', 'password',];

    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'country', 'birthday', 'gender', 'authToken', 'fcm_token', 'vesion', 'email_verified_at', 'confirm_code', 'confirmed', 'password','city','state','street','zip_code',"last_seen",'can_login'];

    protected $hidden = ['password', 'remember_token',];

    public function scopeFilter($query, $filters)
    {
        return Filter::filter($query, $filters, $this->filter_list);
    }

    public function add_points($amount)
    {
        if ($amount > 0) {
            $this->points += $amount;
            $this->save();
        }
    }

    public function points_withdrawal($amount)
    {
        if ($amount <= $this->points) {
            if ($amount > 0) {
                $this->points -= $amount;
                $this->save();
                return 1;
            } else {
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

    public function latestOrder()
    {
        return $this->hasOne(Order::class, 'customer_id')->latestOfMany();
    }

    public function latestVerifiedOrder()
    {
        return $this->hasOne(Order::class, 'customer_id')->where('response', 'VERIFIED')->latestOfMany();
    }

    public function chat_messages()
    {
        return $this->hasMany(ChatMessage::class);
    }

    public function money()
    {
        return $this->hasMany(Money::class);
    }

    public function bank_order()
    {
        return $this->hasMany(bank_enquiry::class,'customer_id');
    }

}
