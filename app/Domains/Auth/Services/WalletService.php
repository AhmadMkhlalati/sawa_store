<?php

namespace App\Domains\Auth\Services;


use App\Domains\Auth\Models\Transaction;
use App\Domains\Auth\Models\Wallet;
use App\Services\BaseService;

/**
 * Class WalletService.
 */

class WalletService extends BaseService
{
    public static function withdrawFunds(Wallet $wallet, float $amount,$moneyId): Transaction
    {


        // Create a new withdrawal transaction record.
        $transaction = new Transaction([
            'money_id' => $moneyId,
            'amount' => $amount,
            'type' => 'withdraw',
        ]);

        $transaction->save();

        $wallet->decrement('balance', $amount);

        return $transaction;
    }

    public function depositFunds(Wallet $wallet, float $amount): Transaction
    {
        // Create a new deposit transaction record.
        $transaction = new Transaction([
            'receiver_id' => $wallet->user_id,
            'amount' => $amount,
            'type' => 'deposit', // You can set the appropriate status here.
        ]);

        $transaction->save();

        $wallet->increment('balance', $amount);

        return $transaction;
    }
}
