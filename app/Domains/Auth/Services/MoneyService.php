<?php

namespace App\Domains\Auth\Services;

use App\Domains\Auth\Events\User\UserCreated;
use App\Domains\Auth\Events\User\UserDeleted;
use App\Domains\Auth\Events\User\UserDestroyed;
use App\Domains\Auth\Events\User\UserRestored;
use App\Domains\Auth\Events\User\UserStatusChanged;
use App\Domains\Auth\Events\User\UserUpdated;
use App\Domains\Auth\Models\Money;
use App\Domains\Auth\Models\Setting;
use App\Domains\Auth\Models\Wallet;
use App\Domains\Auth\Models\WesalMoney;
use App\Domains\Auth\Services\User;
use App\Exceptions\GeneralException;
use App\Http\Livewire\Backend\WesalMoneyTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

/**
 * Class UserService.
 */
class MoneyService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param  Money  $model
     */

    public function __construct(Money $money)
    {
        $this->model = $money;
    }





    /**
     * @param  array  $data
     * @return User
     *
     * @throws GeneralException
     * @throws \Throwable
     */
    public static function store(array $data = []): Money
    {
        DB::beginTransaction();

        try {
            $money = Money::create([
                'beneficiary_name'=>$data['beneficiary_name'],
                'beneficiary_phone'=>$data['beneficiary_phone'],
                'delivery_amount'=>$data['delivery_amount'],
                'reference_number'=>$data['reference_number']
            ]);
            $wallet = Wallet::find(1);
            $usd = Setting::where('key','USD')->first();
            $amount = $data['delivery_amount'] / $usd->value;

//            $response = Http::withHeaders([
//                'Authorization' => 'Bearer PEF1dGhlbnRpY2F0aW9uPjxBY2NvdW50PjE0MjAyMDAxMzQwMjwvQWNjb3VudD48QnJhbmNoSWQ+MzQwMjwvQnJhbmNoSWQ+PFVzZXJOYW1lPndhc2FsVXNkMDAxPC9Vc2VyTmFtZT48UGFzc3dvcmQ+VjBUMnJZZ3NscmthY2MzSW1lVTRPVmwySWFtY1MvWm1qS0tLcncwaTBzWWQwVm01T0N1Y3Bpb3VKa0cwOFhFVWg0d1hsRWU5SU9lWGNaQ05PY2ErbkE9PTwvUGFzc3dvcmQ+PC9BdXRoZW50aWNhdGlvbj4=',
//            ])->post('https://api.sawaex.sy:8443/api/Transaction/TransactionCreate', [
//                "SenderFullName" => "شركة وصال",
//                "SenderNationalityCode" => "OT",
//                "SenderIdentityKindCode" => "National No",
//                "SenderIdentityNumber" => "",
//                "SenderExpirationDate" => "12-09-2023",
//                "SenderKind" => "P",
//                "SenderGender" => "M",
//                "SenderPhoneNumber" => "0955999948",
//                "SenderMobileNumber" => "",
//                "SenderAddress" => "",
//                "SenderBirthdayDate" => "16-09-2005",
//                "SenderPlaceOfBirth" => "",
//                "BeneficiaryFullName" => $data['beneficiary_name'],
//                "BeneficiaryNationalityCode" => "SY",
//                "BeneficiaryGender" => "M",
//                "BeneficiaryKind" => "P",
//                "BeneficiaryPhoneNumber" => "9639" . substr($data['beneficiary_phone'], -8, 8),
//                "BeneficiaryMobileNumber" => "",
//                "BeneficiaryAddress" => "",
//                "BeneficiaryBankName" => "",
//                "BeneficiaryAccountNumber" => "",
//                "RelationShip" => "",
//                "ApiService" => null,
//                "ReferenceNumber" => $data['reference_number'],
//                "CountryCode" => "SY",
//                "CityCode" => "Damascus",
//                "AgentCode" => "1002",
//                "DeliveryCurrencyCode" => "SYP",
//                "DeliveryAmount" => $data['delivery_amount'],
//                "ReasonCode" => "Workers remittances",
//                "SourceOfFund" => "",
//                "AccountCurrencyCode" => null
//            ]);

//            if ($response->successful()) {
            if (true) {
//                $responseData = $response->json();

//                if($responseData['ErrorCode'] == 822){
                    throw new GeneralException($responseData['Message']);
//                }


                $transaction = WalletService::withdrawFunds($wallet, $amount,$money->id);


                $money->transaction_UID = 500000000000;// $responseData['TransactionInfo']['TransactionUID'];
                $token = "Bearer 22783|1Qy84w6vRLB4J7sOat7DLbQznd5dfhoXQLX4KGsu";

                $money->save();

                $responseAdmin = Http::withHeaders([
                    'Authorization' => $token,
                ])->post('https://wesalinternational.com/api/admin/transfer/make_as_payed', [
                    'id'=> $data['reference_number'],
                    'notes' => $money->transaction_UID
                ]);

            } else {
                //$errorMessage = $response->body();
            }


        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException($e->getMessage());
        }

        DB::commit();

        return $money;
    }

}
