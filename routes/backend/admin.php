<?php

use App\Domains\Auth\Http\Controllers\Backend\Money\MoneyController;
use App\Http\Controllers\Backend\DashboardController;
use Tabuna\Breadcrumbs\Trail;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('admin.dashboard'));
    });

Route::group([
    'prefix' => 'money',
    'as' => 'auth.money.',
    'middleware' => 'role:'.config('boilerplate.access.role.admin'),
], function () {
    Route::get('/', [MoneyController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->push(__('Money Management'), route('admin.auth.money.index'));
        });
    Route::get('/wesal-money', [MoneyController::class, 'wesal_money'])
        ->name('wesal.index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->push(__('Wesal Money Management'), route('admin.auth.money.wesal.index'));
        });
    Route::get('/create/{id?}}', [MoneyController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.auth.money.index')
                ->push(__('Create Money Transfer'), route('admin.auth.money.create'));
        });

    Route::post('/store', [MoneyController::class, 'store'])
        ->name('store')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.auth.money.index')
                ->push(__('Money Store'), route('admin.auth.money.store'));
        });


});
